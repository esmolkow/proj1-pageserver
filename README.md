# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### Features ###

  * A tiny Python webserver.
  * Returns a page if a page exists in `DOCROOT`.
  * Returns 404 if page doesn't exist.
  * Returns 403 if the request path begins with `//`,`/~`, or `/..` at any point.

### What do I need?  Where will it work? ###

* Designed for Unix, mostly interoperable on Linux (Ubuntu) or MacOS. May also work on Windows, but no promises. A Linux virtual machine may work, but our experience has not been good; you may want to test on shared server ix-dev.

* You will need Python version 3.4 or higher. 

* Designed to work in "user mode" (unprivileged), therefore using a port number above 1000 (rather than port 80 that a privileged web server would use)

### Usage ###

* Clone the repo.

* Fill in and copy `credentials-skel.ini` to `pageserver/credentials.ini`.

* `make run`